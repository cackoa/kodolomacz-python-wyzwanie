# przykad funkcji
def funkcja_liniowa(x, a, b):
    return a * x + b


# funkcja z wartością domyślną
def potega(podstawa, wykladnik=1):
    i = 1
    wynik = 1
    while i <= wykladnik:
        wynik *= podstawa
        i += 1
    return wynik


# przykład rekurencji
def silnia(n):
    if n == 0:
        return 1
    return n * silnia(n - 1)


if __name__ == "__main__":
    a = 2
    x = 3
    wynik = funkcja_liniowa(x, a, 5)
    print(wynik)
    ## 11
    print(funkcja_liniowa(x, a, 3))
    ## 9

    print(potega(3, 4))
    ## 81

    print(potega(4))
    ## 4

    # wskazywanie argrumentów
    print(potega(wykladnik=4, podstawa=3))
    ## 81

    print(silnia(5))
    ## 120


