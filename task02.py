def main():
    while True:
        print("Podaj w oddzielnych wierszach liczbę, operację matematyczną: +,-,*,/,%, a następnie kolejną liczbę:")
        first_number = input()
        operator = input()
        second_number = input()

        result = None
        f_num = float(first_number)
        s_num = float(second_number)

        if operator == "+":
            result = f_num + s_num
        elif operator == "-":
            result = f_num - s_num
        elif operator == "*":
            result = f_num * s_num
        elif operator == "/":
            result = f_num / s_num
        elif operator == "%":
            result = f_num % s_num

        print("Twój wynik to: " + str(result))

        print("Chcesz wykonać kolejne działanie? Wpisz literę t lub n.")
        again = input()
        if again == "n":
            break


if __name__ == "__main__":
    main()
