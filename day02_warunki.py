# funkcja główna programu
def main():
    wiek = 35
    if wiek >= 18:
        print("osoba jest pełnoletnia")
    ## osoba jest pełnoletnia

    wiek = 15
    if wiek >= 18:
        print("osoba jest pełnoletnia")

    wiek = 15
    if wiek >= 18:
        print("osoba jest pełnoletnia")
    else:
        print("osoba jest niepełnoletnia")
    ## osoba jest niepełnoletnia

    wiek = 19
    if wiek >= 21:
        print("osoba może pić alkohol i posiadać broń")
    elif wiek >= 18:
        print("osoba może pić alkohol")
    else:
        print("osoba nie może pić alkoholu ani posiadać broni")
    ## osoba może pić alkohol


# uruchomienie funkcji głównej
if __name__ == "__main__":
    main()
