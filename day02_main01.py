"""
Tutaj następuje obszerne wyjaśnienie funkcji main().
Przyjmowane parametry: żadne
Wartość zwracana: żadna
"""

# funkcja główna programu
def main():
    print("Witaj świecie!") # ta funkcja wypisuje napis
    # wiersz poniżej wczytuje dane z klawiatury
    pobrany_napis = input()
    print("Twój napis to: " + pobrany_napis)


# uruchomienie funkcji głównej
if __name__ == "__main__":
    main()
