def potega_wydajnie(liczba, wykladnk):
    if wykladnk == 0:
        return 1
    if wykladnk == 1:
        return liczba

    half = int(wykladnk / 2)
    result = potega_wydajnie(liczba, half)
    if wykladnk % 2 == 1:
        return result * result * result
    else:
        return result * result


def czyPalindrom(text):
    for i in range(0, len(text)):
        if text[i] != text[len(text)-i-1]:
            return False
    return True


def czyAnagram(text1, text2):
    text1_set = set(text1)
    text2_set = set(text2)
    return text2_set.issubset(text1_set)


def moda(lista):
    result = {}
    for number in lista:
        if number in result:
            result[number] = result[number] + 1
        else:
            result[number] = 1

    most_frequent = None
    most_count = 0
    for num, count in result.items():
        if count > most_count:
            most_count = count
            most_frequent = num

    return most_frequent

def main():
    print(potega_wydajnie(2, 3))
    print(potega_wydajnie(2, 4))
    print(potega_wydajnie(3, 0))
    print(potega_wydajnie(3, 1))
    print(potega_wydajnie(3, 2))
    print(potega_wydajnie(3, 3))

    print("kajak " + str(czyPalindrom("kajak")))
    print("aabb " + str(czyPalindrom("aabb")))

    print("abc cba: " + str(czyAnagram("abc", "cba")))
    print("abcd cba: " + str(czyAnagram("abcd", "cba")))
    print("abc cbad: " + str(czyAnagram("abc", "cbad")))

    print(moda([1, 2, 3, 3, 4]))
    print(moda([1, 2, 3, 3, 4, 4]))
    print(moda([1]))


if __name__ == "__main__":
    main()
