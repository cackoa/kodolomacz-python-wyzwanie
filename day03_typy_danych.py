
if __name__ == "__main__":
    krotka = (2, 3)
    print(krotka)
    ## (2, 3)

    pierwsza = krotka[0]
    druga = krotka[1]
    print(pierwsza)
    ## 2
    print(druga)
    ## 3

    krotka = (2, "Napis")
    print(krotka)
    ## (2, 'Napis')

    lista = [1, False, "Napis"]
    print(lista)
    ## [1, False, 'Napis']

    print(len(lista))
    ## 3

    lista.append(2.5 + 3.7j)
    print(lista)
    ## [1, False, 'Napis', (2.5+3.7j)]

    print(lista + [7, 8, 9])
    ## [1, False, 'Napis', (2.5+3.7j), 97, 98, 99, 7, 8, 9]

    print([7, 8, 9] * 3)
    ## [7, 8, 9, 7, 8, 9, 7, 8, 9]

    print(lista[2])
    ## Napis

    print(lista[-1])
    ## 99

    print(lista[-2])
    ## 98

    lista[2] = "Nowy"
    print(lista)
    ## [1, False, 'Nowy', (2.5+3.7j), 97, 98, 99]

    print(lista[2:5])
    ## ['Nowy', (2.5+3.7j), 97]

    lista[0:3] = [98, 99, 101, 102]
    print(lista)
    ## [98, 99, 101, 102, (2.5+3.7j), 97, 98, 99]

    lista.insert(1, "Nowy2")
    print(lista)
    ## [98, 'Nowy2', 99, 101, 102, (2.5+3.7j), 97, 98, 99]

    del lista[1]
    print(lista)
    ## [98, 99, 101, 102, (2.5+3.7j), 97, 98, 99]

    liczby = [1, 6, 3, 5, 4, 7]
    liczby.sort()
    print(liczby)
    ## [1, 3, 4, 5, 6, 7]
    liczby.reverse()
    print(liczby)
    ## [7, 6, 5, 4, 3, 1]

    # zbiory

    zbiorPusty = set()
    zbior = {1, 3, 5}
    print(zbiorPusty)
    ## set()
    print(zbior)
    ## {1, 3, 5}

    # czy 1 jest w zbiorze?
    print(1 in zbiorPusty)
    ## False
    print(1 in zbior)
    ## True

    print(1 not in zbiorPusty)
    ## True
    print(1 not in zbior)
    ## False

    zbiorPusty.add(2)
    zbior.add(2)
    print(zbiorPusty)
    ## {2}
    print(zbior)
    ## {1, 2, 3, 5}

    zbiorPusty.discard(2)
    zbior.discard(2)
    print(zbiorPusty)
    ## set()
    print(zbior)
    ## {1, 3, 5}

    print({1, 5, 8} | {1, 5, 9})  # suma
    ## {1, 5, 8, 9}
    print({1, 5, 8} - {1, 5, 9})  # różnica
    ## {8}
    print({1, 5, 8} & {1, 5, 9})  # przecięcie
    ## {1, 5}

    print({1, 5}.issubset({1, 5, 9})) # czy jest podzbiorem?
    ## True

    # Słownik

    bazaDanychPolakow = {"89082911111": ["Jan", "Kowalski", 29],
                         "95092200000": ["Ania", "Nowak", 23],
                         "98122422222": ["Adam", "Mickiewicz", 220]
                         }
    print(bazaDanychPolakow)
    ## {'89082911111': ['Jan', 'Kowalski', 29], '95092200000': ['Ania', 'Nowak', 23], '98122422222': ['Adam', 'Mickiewicz', 220]}

    print("89082911111" in bazaDanychPolakow)
    ## True
    print("95092200022" not in bazaDanychPolakow)
    ## True

    print(bazaDanychPolakow["98122422222"])
    ## ['Adam', 'Mickiewicz', 220]

    print(bazaDanychPolakow.get("89082911111", "wartość domyślna")) # na wypadek gdyby klucza nie było w słowniku
    ## ['Jan', 'Kowalski', 29]
    print(bazaDanychPolakow.get("95092200022", "wartość domyślna"))
    ## wartość domyślna

    # dodanie elementu
    bazaDanychPolakow["88081244444"] = ["Magda", "K", 30]
    print(bazaDanychPolakow)
    ## {'89082911111': ['Jan', 'Kowalski', 29], '95092200000': ['Ania', 'Nowak', 23], '98122422222': ['Adam', 'Mickiewicz', 220], '88081244444': ['Magda', 'K', 30]}

    # usunięcie jednego elementu
    del bazaDanychPolakow["88081244444"]
    print(bazaDanychPolakow)
    ## {'89082911111': ['Jan', 'Kowalski', 29], '95092200000': ['Ania', 'Nowak', 23], '98122422222': ['Adam', 'Mickiewicz', 220]}

    for klucz in bazaDanychPolakow.keys():
        print(klucz)

    for wartosc in bazaDanychPolakow.values():
        print(wartosc)

    for klucz, wartosc in bazaDanychPolakow.items():
        print(klucz)
        print(wartosc)
        print("-----")






    
