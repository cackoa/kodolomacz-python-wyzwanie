# funkcja główna programu
def main():
    for i in range(0, 10):
        print(i)


    i = 0
    while i < 10:
        print(i)
        i += 1

    print("Podaj liczbę ocen, które będą wchodzić w skład średniej")
    liczba_ocen = int(input())
    suma = 0

    for i in range(1, liczba_ocen + 1):
        print("Podaj ocenę " + str(i))
        ocena = float(input())
        suma += ocena

    print("Suma ocen to " + str(suma))
    print("średnia ocen to " + str(suma / liczba_ocen))


# uruchomienie funkcji głównej
if __name__ == "__main__":
    main()
