"""
Tutaj następuje obszerne wyjaśnienie funkcji main().
Przyjmowane parametry: żadne
Wartość zwracana: żadna
"""


# funkcja główna programu
def main():
    x = 7
    y = 3

    suma = x + y
    print("Dodawanie: " + str(suma))

    roznica = x - y
    print("Różnica: " + str(roznica))

    iloczyn = x * y
    print("Iloczyn: " + str(iloczyn))

    iloraz = x / y
    print("Iloraz: " + str(iloraz))

    reszta = x % y
    print("Reszta z dzielenia: " + str(reszta))

    # Operatory łączone

    ziemniaki_w_garnku = 3
    ziemniaki_w_garnku = ziemniaki_w_garnku + 2
    print(ziemniaki_w_garnku)
    ## 5

    ziemniaki_w_garnku = 3
    ziemniaki_w_garnku += 2
    print(ziemniaki_w_garnku)
    ## 5

    logiczna = 4 < 5
    print(logiczna)
    ## True

    logiczna = 6 >= 10
    print(logiczna)
    ## False

    x = 7
    y = 3

    print(str(x) + "<" + str(y) + " == " + str(x < y))
    print(str(x) + "<=" + str(y) + " == " + str(x <= y))
    print(str(x) + ">" + str(y) + " == " + str(x > y))
    print(str(x) + ">=" + str(y) + " == " + str(x >= y))
    print(str(x) + "==" + str(y) + " == " + str(x == y))
    print(str(x) + "!=" + str(y) + " == " + str(x != y))

    wiek = 35
    czy_wiek_produkcyjny = 18 <= wiek <= 65
    print(czy_wiek_produkcyjny)
    ## True

    print("1 i 1: " + str(True and True))
    ## 1 i 1: True
    print("0 i 1: " + str(False and True))
    ## 0 i 1: False
    print("1 i 0: " + str(True and False))
    ## 1 i 0: False
    print("0 i 0: " + str(False and False))
    ## 0 i 0: False

    # or

    print("1 lub 1: " + str(True or True))
    ## 1 lub 1: True
    print("0 lub 1: " + str(False or True))
    ## 0 lub 1: True
    print("1 lub 0: " + str(True or False))
    ## 1 lub 0: True
    print("0 lub 0: " + str(False or False))
    ## 0 lub 0: False

    # not
    print("nie 1: " + str(not True))
    ## nie 1: False
    print("nie 0: " + str(not False))
    ## nie 0: True

    # <=
    wiek = 35
    czy_wiek_produkcyjny = 18 <= wiek and wiek <= 65
    print(czy_wiek_produkcyjny)
    ## True


# uruchomienie funkcji głównej
if __name__ == "__main__":
    main()
