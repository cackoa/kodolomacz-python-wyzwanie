
if __name__ == "__main__":
    napis = "Ala ma kota"
    print(napis[0])
    ## A
    print(napis[0:3])
    ## Ala

    print(len(napis))
    ## 11

    print("Pierwszy wiersz\nDrugi wiersz")
    ## Pierwszy wiersz
    ## Drugi wiersz

    print(r"Pierwszy wiersz\nDrugi wiersz")
    ## Pierwszy wiersz
    # Drugi wiersz

    zmienna = 7
    napis = f"wartość zmiennej to {zmienna}"
    print(napis)
    ## wartość zmiennej to 7

    wieleWierszy = """Tutaj pierwszy
    a tu drugi
    tutaj trzeci"""
    print(wieleWierszy)
    ## Tutaj pierwszy
    ## a tu drugi
    ## tutaj trzeci

    print("a" < "b")
    ## True

    napis = "Ala ma kota."
    print(napis.upper())
    ## ALA MA KOTA.

    napis = "Ala Ma Kota."
    print(napis.lower())
    ## ala ma kota.

    napis = "   \n\t Ala \n "
    print(napis.strip())
    ## Ala

    napis = "wyraz, inny wyraz, i jeszcze inny wyraz"
    print(napis.count("wyraz"))
    ## 3
    napis = napis.replace("wyraz", "WYRAZ")
    print(napis)
    ## WYRAZ, inny WYRAZ, i jeszcze inny WYRAZ

    listaNapisow = ["Ala", "ma", "kota"]
    print("+".join(listaNapisow))
    ## Ala+ma+kota

    napis = "Ala ma kota"
    print(napis.split(" "))
    ## ['Ala', 'ma', 'kota']



    



    
