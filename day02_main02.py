"""
Tutaj następuje obszerne wyjaśnienie funkcji main().
Przyjmowane parametry: żadne
Wartość zwracana: żadna
"""


# funkcja główna programu
def main():
    a = 7
    print(a)
    # 7
    a = 8
    print(a)
    # 8
    podatek_VAT = 0.23

    logiczna = True
    print(type(logiczna))

    calkowita = -7
    print(type(calkowita))

    zmiennopozycyjna = -7.4
    print(type(zmiennopozycyjna))

    zespolona = -7.4 + 8.4j
    print(type(zespolona))

    napis = "6+7j"  # równie dobrze "ala ma kota"
    print(type(napis))



    # Konwersje typów

    print(float(5))
    ## 5.0

    print(int(5.0))
    ## 5

    print(int(5.7))
    ## 5

    print(int(-5.7))
    ## -5

    print(float("-5.3"))
    ## -5.3

    napis = str(-5.3)
    print(napis)
    ## -5.3
    print(type(napis))

    # operatory




# uruchomienie funkcji głównej
if __name__ == "__main__":
    main()
